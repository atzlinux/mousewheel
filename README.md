## 鼠标滚轮速度设置程序 mousewheel
该程序可以设置鼠标滚轮每滚动一齿所移动的距离。

在程序菜单 设置 子菜单下启动设置界面或者搜索 mousewheel 启动程序。

![程序界面截图](mousewheel.png)

最小值为 1 ，最大值为 100。
1 个单位约对应命令行终端里面的 3 行距离。

## 使用场景
该软件包在底层调用了 imwheel 软件包，使用 zenity 来展示界面。
能够同时支持大部分 Linux 桌面环境，Xfce、Gnome、KDE、Mate 等。

## 安装
该软件已经由[铜豌豆 Linux](https://www.atzlinux.com) 项目率先制作成 deb 包，
在添加[铜豌豆 Linxu  软件源](https://www.atzlinux.com/allpackages.htm)后，可以用软件包名称 mousewheel 安装。

命令行：

apt install mousewheel

该软件包同时支持多个 CPU 架构。

## 本软件是参考下面网址的脚本修改打包

https://nicknorton.net/?q=node/4

感谢 Nick！
